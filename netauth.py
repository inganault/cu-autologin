#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import re,os
import time
import config

warning = '''
** คำแนะนำ **
กรุณาค้างหน้าจอนี้ไว้ตลอดการใช้งาน
กรุณาเปิดหน้าจอใหม่และป้อน URL เพื่อไปยังหน้าเว็บที่ต้องการ
กดปุ่ม Ctrl+C ทุกครั้งที่เลิกการใช้งาน
Press Ctrl+C when stop using.
'''.strip()

connected_regex = r'Connected'
userid_regex = r'<td width="200">([0-9]+)</td>'
errmsg_regex = r'id="errMessage">([^<]*?)</div>'

sess = requests.session()
sess.cookies = requests.cookies.cookiejar_from_dict(config.cookies)
sess.headers.update({'referer': 'https://netauth.it.chula.ac.th/user/Logon.do'})


def tcping(host, port, maxCount=4, timeout=1, wait=1):
    import socket
    from timeit import default_timer as timer
    passed_count = 0
    for count in range(maxCount):
        success = False
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(timeout)
        runtime = -timer()
        try:
            s.connect((host, int(port)))
            s.shutdown(socket.SHUT_RD)
            success = True
        except socket.timeout:
            print("Connection timed out")
        except OSError as e:
            print("OS Error:", e)
        runtime += timer()

        if success:
            print("Connected to %s[%s]: seq=%s time=%.2f ms" % (host, port, count+1, 1000 * runtime))
            passed_count += 1
        if count < maxCount-1:
            time.sleep(wait)
    return passed_count > 0

def httpping(host,_, count=2, timeout=1, wait=1):
	from timeit import default_timer as timer
	if not (host.startswith("http://") or host.startswith("https://")):
		host = 'http://'+host
	for i in range(count):
		try:
			runtime = -timer()
			resp = requests.get(host,timeout=timeout)
			runtime += timer()
			if(resp.status_code == 200):
				print('Ping to %s OK [%.2fms]'%(host,runtime*1000))
				return True
		except Exception:
			print('Ping to %s failed'%host)
			pass
		time.sleep(wait)
	return False

altping = {'tcp':tcping,'http':httpping}
def ping(hostname, port):
	if config.ping_method not in altping:
	    response = os.system("ping -c 4 " + hostname)
	    return response == 0
	else:
		return altping[config.ping_method](hostname, port)

def check_network(i = 0):
	hosts = [('netauth.it.chula.ac.th', 80), config.ping_ext_host]
	for i in range(i+1):
		if not ping(*hosts[i]):
			break
	else:
		return True
	return False


def get_userid():
	resp = sess.get('http://netauth.it.chula.ac.th/user/Logon/logoff.jsp', allow_redirects=False)
	if resp.status_code == 302:
		return None
	if resp.status_code == 200:
		if re.search(connected_regex, resp.text) is None:
			return None
		userid = re.search(userid_regex, resp.text) 
		if userid is None:
			return None
		return userid.group(1)
	raise Exception('Network error')

def keepalive():
	resp = sess.post('http://netauth.it.chula.ac.th/user/KeepAlive.do', data={'cmd':'keepAlive','_':''})
	if resp.status_code == 200 and resp.text == 'OK':
		return True
	return False

def login(username, password, captcha):
	resp = sess.post('https://netauth.it.chula.ac.th/user/Logon.do', data={
		'cmd':'logon',
		'userName':username,
		'password':password,
		'verificationCode':captcha,
		'chkAccept':'on',
		'realmID':'1'
		}, allow_redirects=False)
	if resp.status_code == 302:
		return True, ''
	if resp.status_code == 200:
		errmsg = re.search(errmsg_regex, resp.text)
		if errmsg is not None:
			return False, errmsg.group(1)
		return False, ''
	return False, ''

def logout():
	sess.post('https://netauth.it.chula.ac.th/user/Logon.do', data={'cmd': 'logoff'}, allow_redirects=False)

def get_captcha():
	resp = sess.get('https://netauth.it.chula.ac.th/user/Image.do')
	if resp.status_code != 200:
		return None
	return resp.content

def solve_captcha_manual(captcha):
	from PIL import Image
	import io
	Image.open(io.BytesIO(captcha)).show()
	return input('Solve this captcha:')

def preamble():
	sess.get('https://netauth.it.chula.ac.th/user/Logon.do')

if __name__ == '__main__':
	import ocr
	from io import BytesIO
	path = os.path.dirname(os.path.abspath(__file__))
	path_classify = path + '/classified'
	ocr_vectors = ocr.load_vectors(path = path_classify)
	try:
		while True:
			ck_network = check_network()
			if ck_network:
				preamble()
				for i in range(config.login_retry_count):
					for j in range(config.captcha_retry_count):
						captcha = get_captcha()
						if captcha is None:
							raise Exception('Cant get captcha')
						
						captcha = ocr.recognize(ocr_vectors, BytesIO(captcha))
						if captcha is None:
							print('Can\'t solve captcha retrying... (%d)'%(j+1))
							continue
						break
					else:
						raise Exception('Unable to solve captcha')
					
					print('Logging in... ', end='')
					success, msg = login(config.username, config.password, captcha)
					if success:
						print('[OK]')
						break
					
					else:
						print('[Fail]\nError:',msg)
						print('Retrying... (%d)'%(i+1))
				
				else:
					raise('Can\'t login')

				userid = get_userid()
				print('Logged in with user:', userid)
				if userid != config.username:
					raise Exception('Username not matched')

				print(warning)
				keepalive_count = 0
				while True:
					for k in range(config.time_interval*10):
						time.sleep(0.1)
					
					ck_network = check_network(1)
					if not ck_network:
						break

					success = keepalive()
					if not success:
						print('\nKeepalive failed')
						break

					keepalive_count += 1
					print('Sending Keepalive [%d]' % keepalive_count, end='\r')
			else:
				for k in range(config.time_interval):
						time.sleep(0.1)

	except KeyboardInterrupt:
		pass
	except Exception as ex:
		print('Exception:',ex)
	finally:
		print('\nLogging out')
		logout()
