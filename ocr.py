from collections import Counter, defaultdict
from PIL import Image
from os import listdir
from os.path import join

min_char_pixel_count = 10

directions = [(0,1),(0,-1),(1,0),(-1,0)]
palette = [
(0,0,0),
(91,169,102),
(199,90,147),
(172,151,62),
(129,118,204),
(204,95,67),
(255,255,255)]

def load_vectors(chars_list = '0123456789abcdef', path='classified'):
	chars_list = list(chars_list)
	vectors = []
	for i in chars_list:
		dirn = join(path,i)
		for fn in listdir(dirn):
			im = Image.open(join(dirn,fn))
			vectors.append((list(im.getdata()),i))
			im.close()
	return vectors

def solve(vectors, im): # size = 10x10
	res = []
	for angle in [-30,-20,-10,0,10,20,30]:
		cur = list(im.rotate(angle,Image.BILINEAR).resize((10,10), Image.BILINEAR).getdata())
		for vec, char in vectors:
			corr = sum(((i-j)/255)**2 for i,j in zip(cur,vec))**.5
			res.append((corr,char))
	return min(res)[1]

save_counter = 0
def recognize(vectors, fp, verbose = False, savemode = False):
	global save_counter
	im = Image.open(fp)
	try:
		w,h = im.size
		pix = im.load()

		histrogram = Counter((pix[x,y] for x in range(w) for y in range(h)))
		colors = [i[0] for i in histrogram.most_common(2)]

		char_color = None
		for clr in colors:
			if any(pix[0,y] == clr or pix[w-1,y] == clr for y in range(h)):
				continue
			if any(pix[x,0] == clr or pix[x,h-1] == clr for x in range(w)):
				continue
			char_color = clr
		if char_color is None:
			if verbose:
				print('Error: Character at edge')
			return None

		visited = [[0]*w for i in range(h)]
		char = defaultdict(set)

		def fill(x, y, id):
			stack = [(x,y)]
			count = 0
			while stack:
				x,y = stack.pop()
				try:
					if visited[y][x] or pix[x,y] != char_color:
						continue
				except IndexError:
					continue
				visited[y][x] = id
				char[id].add((x,y))
				count += 1
				for dx,dy in directions:
					stack.append((x+dx, y+dy))
			return count

		for x in range(w):
			for y in range(h):
				if 0 < fill(x,y,len(char)+1) < min_char_pixel_count:
					for x,y in char[len(char)]:
						visited[y][x] = -1
					del char[len(char)]
		if len(char) != 5:
			if verbose:
				print('Error: character count = ',len(char))
			return None

		## Segmentation result
		# im2 = Image.new(im.mode, im.size)
		# pix2 = im2.load()
		# for y in range(h):
		# 	for x in range(w):
		# 		pix2[x,y] = palette[visited[y][x]]
		# im2.show()
		# im2.close()

		result = ''
		for k in char:
			ch = char[k]
			minx,maxx = min(i[0] for i in ch),max(i[0] for i in ch)
			miny,maxy = min(i[1] for i in ch),max(i[1] for i in ch)
			cw,ch = (maxx-minx+1,maxy-miny+1)
			imsize = max(cw,ch)
			ch_img = Image.new('L', (imsize,imsize))
			ch_pix = ch_img.load()
			offx,offy = (imsize-cw)//2,(imsize-ch)//2

			for y in range(ch):
				for x in range(cw):
					ch_pix[x+offx,y+offy] = (visited[y+miny][x+minx] == k) * 255
			result += solve(vectors, ch_img)
			if savemode:
				ch_img.resize((10,10), Image.BILINEAR).save('save/%d.png'%save_counter)
				save_counter += 1
			ch_img.close()

		return result
	finally:
		im.close()
	return None

if __name__ == '__main__':
	import netauth
	from io import BytesIO
	vectors = load_vectors()
	count = 50
	while count:
		print('Get captcha')
		captcha = netauth.get_captcha()
		print('Processing...')
		result = recognize(vectors, BytesIO(captcha), verbose = True)
		if result:
			print('Out:',result)
			im = Image.open(BytesIO(captcha))
			# im.show()
			im.close()
			count -= 1